
public class Electrodomestico {

	private final String COLOR_DEFECTO = "blanco";
	private final char CONSUMO_DEFECTO = 'F';
	private final double PRECIO_DEFECTO = 100.0;
	private final double PESO_DEFECTO = 5.0;
	
	private double precioBase;
	private String color;
	private char consumoEnergetico;
	private double peso;
	
	public Electrodomestico() {
		this.precioBase = PRECIO_DEFECTO;
		this.color = COLOR_DEFECTO;
		this.consumoEnergetico = CONSUMO_DEFECTO;
		this.peso = PESO_DEFECTO;
	}
	
	public Electrodomestico(double precioBase, double peso) {
		this.precioBase = precioBase;
		this.peso = peso;
		this.color = COLOR_DEFECTO;
		this.consumoEnergetico = CONSUMO_DEFECTO;
	}
	
	public Electrodomestico(double precioBase, String color, char consumo, double peso) {
		this.precioBase = precioBase;
		this.peso = peso;
		if(color!=COLOR_DEFECTO && color!="negro" && color!="rojo" && color!="azul" && color!="gris") {
			System.out.println("Error en la introducción del color, se usar� el color por defecto");
			this.color = COLOR_DEFECTO;
		}else {
			this.color = color;
		}
		if(consumo >= 'A' && consumo <= 'F') {
			this.consumoEnergetico = consumo;
		}else {
			System.out.println("Error en la introducci�n del consumo, se usar� el consumo por defecto");
			this.consumoEnergetico = CONSUMO_DEFECTO;
		}
	}
	
	public String getColor() {
		return color;
	}
	
	public void setColor(String color) {
		if(color!=COLOR_DEFECTO && color!="negro" && color!="rojo" && color!="azul" && color!="gris") {
			System.out.println("Error en la introducci�n del color, se usar� el color por defecto");
			this.color = COLOR_DEFECTO;
		}else {
			this.color = color;
		}
	}
	
	public char getConsumo() {
		return consumoEnergetico;
	}
	
	public void setConsumo(char consumo) {
		if(consumo >= 'A' && consumo <= 'F') {
			this.consumoEnergetico = consumo;
		}else {
			System.out.println("Error en la introducci�n del consumo, se usar� el consumo por defecto");
			this.consumoEnergetico = CONSUMO_DEFECTO;
		}
	}
	
	public double getPrecio() {
		return precioBase;
	}
	
	public void setPrecio(double precio) {
		this.precioBase = precio;
	}
	
	public double getPeso() {
		return peso;
	}
	
	public void setPeso(double peso) {
		this.peso = peso;
	}

}
