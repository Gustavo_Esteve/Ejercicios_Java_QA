public class Password {
	private int longitud;
	private String contrase�a;
	
	private char letras[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','�','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9'};

	public Password() {
		longitud = 8;
		contrase�a = "";
		for(int i = 0; i < 8; i++) {
			contrase�a = contrase�a + (letras[(int) (Math.random()*36)]);
		}
	}
	
	public Password(int longitud) {
		this.longitud = longitud;
		contrase�a = "";
		for(int i = 0;i<longitud;i++) {
			contrase�a = contrase�a + (letras[(int) (Math.random()*36)]);
		}
	}
	
	public int getLongitud() {
		return longitud;
	}
	
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	
	public String getContrase�a() {
		return contrase�a;
	}
}
