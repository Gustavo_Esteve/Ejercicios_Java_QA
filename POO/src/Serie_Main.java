
public class Serie_Main {

	public static void main (String[]args) {
		
		Serie s1 = new Serie ("Mr.robot","Tolkien");
		System.out.println(s1.getTitulo()+" -"+s1.getCreador());
		Serie s2 = new Serie ("Mr.robot",'3',"Tolkien","Drama");
		System.out.println(s2.getTitulo()+" -"+" "+s2.getTemporadas()+" "+s2.getCreador()+" "+s2.getGenero());
	}

}
