import javax.swing.JOptionPane;
public class Password_Main {

    public static void main(String[] args) {
        
        String opcion = JOptionPane.showInputDialog("¿Quieres introducir la longitud?");
            if(opcion.toLowerCase().equals("si")) {
                int longitud = Integer.parseInt(JOptionPane.showInputDialog("Introduce la longitud de la contraseña: "));
                Password c = new Password(longitud);
                JOptionPane.showMessageDialog(null, "La contraseña generada es: " + c.getContraseña());
            }else if(opcion.toLowerCase().equals("no")) {
                Password c = new Password();
                JOptionPane.showMessageDialog(null, "La contraseña generada es: " + c.getContraseña());
            }else {
                JOptionPane.showMessageDialog(null, "Opción incorrecta, se generará contraseña por defecto.");
                Password c = new Password();
                JOptionPane.showMessageDialog(null, "La contraseña generada es: " + c.getContraseña());
            }
    }
}