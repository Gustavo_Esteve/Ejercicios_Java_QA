
public class Persona_Main {

	
	public static void main (String[]args) {
		
		Persona p1 = new Persona("48763298D");
		System.out.println(p1.getDni());
		String minombre = "Gustavo";
		Persona p2 = new Persona(minombre, 18, 'H');
		System.out.println(p2.getNombre( )+" "+p2.getEdad( )+" "+p2.getSexo( ));
		Persona p3 = new Persona (minombre, 18, "418565485D", 'H' , 62.4,1.98);
		System.out.println(p3.getNombre()+"  "+ p3.getEdad()+"  "+p3.getDni()+"  "+p3.getSexo()+"  "+p3.getPeso()+" Kg  "+ p3.getAltura()+" m ");
	}
	
		
}
